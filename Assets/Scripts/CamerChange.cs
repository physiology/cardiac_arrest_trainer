﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerChange : MonoBehaviour {
    public float speed = 0.5f;
    private float X;
    private float Y;

    // Use this for initialization
    void Start () {
        print("It's starting!");
	}

    private void Update()
    {
        if (Input.GetKeyDown("c"))
        {
            OnKeyDown();
        } 
        if (Input.GetMouseButton(1))
        {
            transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * speed, -Input.GetAxis("Mouse X") * speed, 0));
            X = transform.rotation.eulerAngles.x;
            Y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(X, Y, 0);
        }

    }
    private void OnKeyDown()
    {
        transform.Rotate(new Vector3Int(90, 0, 0));
    }

    private void OnMouseUp()
    {
        transform.Rotate(new Vector3Int(-90, 0, 0));
    }
}
