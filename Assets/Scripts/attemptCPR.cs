﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class attemptCPR : MonoBehaviour {
    GameObject cprHands;
    GameObject placeButton;
    GameObject canvas;
    public Text notify;
	// Use this for initialization
	void Start () {
        cprHands = GameObject.Find("glove_hands");
        placeButton = GameObject.Find("Placer");
        canvas = GameObject.Find("Canvas");
        Text[] texts = canvas.GetComponentsInChildren<Text>();
        notify = texts[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void placementCorrect(){
        Vector3 placement = cprHands.transform.position;
        
        if (placement.x >= -5.78f && placement.x <= -5.73f &&
            placement.z >= -23.80f && placement.z <= -23.66f)
        {
            notify.text = "Placement Correct! Start CPR!";
            placeButton.SetActive(false);
        }
        else
        {
            notify.text = "Placement Incorrect! Try again." +
                " (Hint: Hands should be in the center of the chest just below where the ribs meet)";
        }
    }
}
