﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class attemptShock : MonoBehaviour {
    GameObject shockpad1;
    GameObject shockpad2;
    GameObject shockButton;
    GameObject shockpads;
    GameObject canvas;
    public Text notify;
    // Use this for initialization
    void Start () {
        shockpad1 = GameObject.FindGameObjectWithTag("Shockpad1");
        shockpad2 = GameObject.FindGameObjectWithTag("Shockpad2");
        shockpads = GameObject.Find("blender_pads");
        shockButton = GameObject.Find("Shocker");
        canvas = GameObject.Find("Canvas");
        Text[] texts = canvas.GetComponentsInChildren<Text>();
        notify = texts[0];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlacementCorrect()
    {
        Vector3 placement1 = shockpad1.transform.position;
        Vector3 placement2 = shockpad2.transform.position;



        if (placement1.x >= -6.63f && placement1.x <= -6.52f &&
             placement1.z >= -23.78f && placement1.z <= -23.62f)
        {
            if (placement2.x >= -6.76f && placement2.x <= -6.71f &&
            placement2.z >= -23.70f && placement2.z <= -23.57f)
            {
                notify.text = "Placement Correct! Shock Successful";
                shockButton.SetActive(false);
                shockpads.SetActive(false);

            }
            else
            {
                notify.text = "Placement Incorrect! Try again." +
                    " (Hint: Red pad should be on right side of patient, below the heart)";

            }
        }
        else
        {
            notify.text = "Placement Incorrect! Try again." +
                " (Hint: Green pad goes on center-left part of chest, above the heart)";

        }
    }
}
