﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class heartflash : MonoBehaviour
{

    public Image HeartBleed;
    public bool flashOn = true;
    public float timer = 0f;
    public bool upSwing = true;

    public IEnumerator flash()
    {
        while (true)
        {
            if (flashOn) { HeartBleed.SetTransparency(1); }
            else { HeartBleed.SetTransparency(0f); }
            flashOn = !flashOn;

            yield return new WaitForSeconds(.28985f);
        }

    }

    void Start()
    {
        HeartBleed = GetComponent<Image>();
        Color c = HeartBleed.color;
        c.a = 0;
        HeartBleed.color = c;
        StartCoroutine(flash());
        HeartBleed.SetTransparency(1);
    }

    // Update is called once per frame
    void Update()
    { }
}

public static class Extensions
{
    public static void SetTransparency(this UnityEngine.UI.Image p_image, float p_transparency)
    {
        if (p_image != null)
        {
            UnityEngine.Color __alpha = p_image.color;
            __alpha.a = p_transparency;
            p_image.color = __alpha;
        }
    }
}