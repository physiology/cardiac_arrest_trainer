﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pulse : MonoBehaviour
{


    //heart.rectTransform.sizeDelta = new Vector2(_currentScale, _currentScale);

    public Image heart;
    public bool flashOn = true;
    public float timer = 0f;
    public bool upSwing = true;
    public IEnumerator Beat()
    {
        while (true)
        {
    
            yield return new WaitForSeconds(.28985f);
        }

    }

    private void Start()
    {
        heart = GetComponent<Image>();
        Color c = heart.color;
        c.a = 0;
        heart.color = c;
       
        StartCoroutine(Beat());
    }

    // Update is called once per frame
    void Update()
    {
           timer += Time.deltaTime;
           if (timer >= .28985f)
            {
                timer = 0f;
                upSwing = !upSwing;
            }
           if (timer >= .15f && Input.GetKeyDown("space")) 
               { 
                heart.GetComponent<Image>().color = new Color32(255, 255, 255, 255); 
               }
           if (timer < .15f) { heart.GetComponent<Image>().color = new Color32(125, 52, 52, 255); }
           
    }
}