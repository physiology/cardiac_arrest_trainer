﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class supTachButton : MonoBehaviour {

    public void NewGameBtn(string newGameLevel)
    {
        saveScene.levelToLoad = "End_Scene_SupTach";
        SceneManager.LoadScene(newGameLevel);
    }
}
