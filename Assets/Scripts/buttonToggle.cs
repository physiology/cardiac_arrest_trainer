﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonToggle : MonoBehaviour {

    // Update is called once per frame
    void Update()
    {

    }

    GameObject shockButton;
    GameObject placeButton;
    void Start()
    {
        shockButton = GameObject.Find("Shocker");
        placeButton = GameObject.Find("Placer");
    }
    public void EnableShock()
    {
        shockButton.SetActive(true);
    }

    // function to disable
    public void DisableShock()
    {
        shockButton.SetActive(false);
    }
    public void EnableHands()
    {
        placeButton.SetActive(true);
    }

    // function to disable
    public void DisableHands()
    {
        placeButton.SetActive(false);
    }
}
