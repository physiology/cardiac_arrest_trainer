﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjects : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		GameObject pads = GameObject.Find("blender_pads");
		GameObject glove_hands = GameObject.Find("glove_hands");
        GameObject shockButton = GameObject.Find("Shocker");
        GameObject handButton = GameObject.Find("Placer");
        //For future implementation
        // GameObject x = GameObject.Find(" y ");
        // Where x is the variable name for the object, and y is the file name
        pads.SetActive(!pads.activeSelf);
		glove_hands.SetActive (!glove_hands.activeSelf);
        shockButton.SetActive(false);
        handButton.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
