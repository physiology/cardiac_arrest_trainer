﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;


public class WeightReading : MonoBehaviour {
	SerialPort sp = new SerialPort("COM8", 9600);
	public string weight;
	public string[] dataArray;
	public string data;
	public Text weightV;
	// Use this for initialization
	void Start () {
		GameObject canvas = GameObject.Find("Canvas");
		Text[] textValue = canvas.GetComponentsInChildren<Text>();
		weightV = textValue[0];
		sp.Open();

	}
	
	// Update is called once per frame
	void Update () {
		try{
			data = sp.ReadLine();
			dataArray = data.Split(',');
			weight = dataArray[1];
			weightV.text = weight;
            new WaitForSeconds(0.05f);
		}
		catch(System.Exception){
		}
	}
}
