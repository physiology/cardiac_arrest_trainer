﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenPrev : MonoBehaviour {

    string prevLevel = saveScene.levelToLoad;

    public void loadScene()
    {
        SceneManager.LoadScene(prevLevel);
    }
}
