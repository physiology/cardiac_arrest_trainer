﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class bradButton : MonoBehaviour {

    public void NewGameBtn(string newGameLevel)
    {
        saveScene.levelToLoad = "End_Scene_Brad";
        SceneManager.LoadScene(newGameLevel);
    }
}
