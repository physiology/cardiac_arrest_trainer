﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shockUp : MonoBehaviour {
    GameObject button;
    void Start()
    {
        button = GameObject.Find("Shocker");
    }
    public void EnableButton()
    {
        button.SetActive(true);
    }

    // function to disable
    public void DisableButton()
    {
        button.SetActive(false);
    }

}
