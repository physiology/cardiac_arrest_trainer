﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraAngle : MonoBehaviour {
    Vector3 cameraPosition1 = new Vector3(-5.29f, 0.26f, -24f);
    Vector3 cameraPosition2 = new Vector3(-6.5f, 1.11f, -23.57f);
    Quaternion camRotation2 = new Quaternion(0.5f, -0.5f, 0.5f, 0.5f);
    Quaternion camRotation1 = new Quaternion(0.0f, -0.7f, 0.0f, 0.7f);
    GameObject cprHands;
    GameObject placeButton;
    GameObject shockpads;
    GameObject shockButton;
    public int camPosition;
    public float speed = 0.5f;
    private float X;
    private float Y;
    // Use this for initialization
    void Start () {
        camPosition = 2;
        this.transform.position = cameraPosition2;
        shockButton = GameObject.Find("Shocker");
        shockpads = GameObject.Find("blender_pads");
        placeButton = GameObject.Find("Placer");
        cprHands = GameObject.Find("glove_hands");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("c")) 
        {
            print(this.transform.rotation);
            changeCameraView();
        }
        if (Input.GetMouseButton(1) && camPosition == 1)
        {
            transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * speed, -Input.GetAxis("Mouse X") * speed, 0));
            X = transform.rotation.eulerAngles.x;
            Y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(X, Y, 0);
        }
    }

    private void changeCameraView()
    {
        if (camPosition == 1)
        {
            camPosition = 2;
            this.transform.position = cameraPosition2;
            this.transform.rotation = camRotation2;
        } else
        {
            camPosition = 1;
            this.transform.position = cameraPosition1;
            this.transform.rotation = camRotation1;
            cprHands.SetActive(false);
            shockpads.SetActive(false);
            shockButton.SetActive(false);
            placeButton.SetActive(false);
        }
    }
}
