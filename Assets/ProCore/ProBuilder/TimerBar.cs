﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimerBar : MonoBehaviour {
    public float currentTime { get; set; }
    public float maxTime { get; set; }
    public int state = 1;
    public Slider timerBar;

	// Use this for initialization
	void Start ()
    {
        maxTime = 100;
        currentTime = 0;

        timerBar.value = calculateTime();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (currentTime < maxTime && state == 1)
        {
            currentTime += 120 * Time.deltaTime;
            if (currentTime >= maxTime)
            {
                currentTime = maxTime;
                state = 2;

            }
        }

        if (currentTime > 0 && state == 2)
        {
            currentTime -= 120 * Time.deltaTime;
            if (currentTime <= 0)
            {
                currentTime = 0;
                state = 1;
            }
        }

        timerBar.value = calculateTime();
	}

    float calculateTime()
    {
        return currentTime / maxTime;
    }
}
